# Backup

This script is designed to backup files using `rsync`.
For example, backing up all photos from android device to remote server.
It's just a bash script which can be run in a contained environment,
like when using [Termux][termux].

## Termux

After installing [Termux][termux], you can use the package manager `pkg` to install dependencies.

Note at the time of this writing, the best way to install Termux is via Github release assets.
It's pretty easy, navigate to latest release from phone, download, enable Android perms, install.
Play Store version is woefully outdated, and no one can find the guy with access to the account.

Clone this repository to install the backup script.

Finally you'll need to [setup storage][storage] in order to share photos and other files with Termux app.

```bash
pkg update
pkg install git
pkg install rsync
git clone https://gitlab.com/jpuck/backup-phone.git
termux-setup-storage
```

### SSH

If you're connecting to a remote server, do it securely and [generate a key pair first][ssh].

```bash
ssh-keygen -t ed25519 -C "your_email@example.com"
eval "$(ssh-agent -s)"
ssh-add /data/data/com.termux/files/home/.ssh/id_ed25519
ssh-copy-id -i /data/data/com.termux/files/home/.ssh/id_ed25519.pub "your_username@server.example.com"
```

If you're connecting with a user account that doesn't have a password,
then you'll have to manually copy the public key to `authorized_keys` on the server.

## Invocation

```bash
SOURCE="/data/data/com.termux/files/home/storage/dcim/Camera"
DESTINATION="your_username@server.example.com:/some/storage"
./main.bash "${SOURCE}" "${DESTINATION}"
```

You can even hard-code this as an alias in `.bashrc`

```bash
alias backup="/data/data/com.termux/files/home/backup-phone/main.bash /data/data/com.termux/files/home/storage/dcim/Camera your_username@server.example.com:/some/storage"
```

[termux]:https://termux.dev/en/
[ssh]:https://docs.github.com/en/authentication/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent
[storage]:https://wiki.termux.com/wiki/Internal_and_external_storage
