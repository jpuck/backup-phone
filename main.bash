#!/bin/bash

# stop on any error
set -e

# usage
if [ ! $# -eq 2 ]; then
    echo "usage: ./main.bash {SOURCE_DIRECTORY} {DESTINATION_DIRECTORY}"
    exit 1
fi

# args
SOURCE="$1"
DESTINATION="$2"
CONFIGURATION_DIRECTORY="$DESTINATION/.backup"
LOG_FILE="$CONFIGURATION_DIRECTORY/backup.lst"

# https://unix.stackexchange.com/a/84980/148062
tmpdir=`mktemp -d 2>/dev/null || mktemp -d -t 'tmpdir'`
tmplog="$tmpdir/backup.lst"

# explain
set -x

# get a local copy of the excludes file
rsync -avz "$LOG_FILE" "$tmpdir"

# copy new files destination
rsync -az --omit-dir-times --exclude-from="$tmplog" --out-format="%n" "$SOURCE/" "$DESTINATION" | tee -a "$tmplog"

# save state
rsync -avz --progress "$tmplog" "$CONFIGURATION_DIRECTORY"
